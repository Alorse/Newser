function aos_main(){
	var self = this;
    /**
     * [_init Se encarga de inicializar los métodos necesarios para el arranque el app y de validar el S.O]
     * @return {[type]} [description]
     */
    this._init = function(){
    	self.initialize();
    	self.actions();
    };
    /**
     * [initialize Inicializa Angular JS y activa la goereferenciación]
     * @return {[type]} [description]
     */
    this.initialize = function(){
        var newser = angular.module('newser', ['parse-angular', 'ngRoute', 'infinite-scroll']);
        _controllers.get(newser);
    };
    /**
     * [actions Inicializa los eventos de Click que sea necesario definir]
     * @return {[type]} [description]
     */
    this.actions = function(){
    	$(document).on("click", ".closeSession", function() {
			Parse.User.logOut();
			window.location.href = '#/';
            window.location.reload();
		});
        $('.loading_fullscreen').css('padding-top',$(document).height()/2);
        $(window).resize(function() {
            $('.loading_fullscreen').css('padding-top',$(document).height()/2);
        });
        self.textareaActions();
    };
    /**
     * [textareaActions Ejecuta los eventos que tendrá el cuadro de texto, transiciones y scripts del mismo]
     * @return {[type]} [description]
     */
    this.textareaActions = function(){
        $(document).on("focus", ".news textarea", function(){
            $('.panel-footer').addClass("show");
            $(this).css('height', '80px');
        });
        $(document).on("blur", ".news textarea", function(){
            $('.panel-footer').removeClass("show");
        });
        $(document).on("keyup", ".news textarea", function(){
            var count = $(this).val().length;
            if (count >= 256) {
                $(this).val($(this).val().substring(0, 256));
            }
            $('.counttxt').text(256 - count);
            if(count <=2){
                $('input#submit').prop('disabled', true);
            } else{
                $('input#submit').prop('disabled', false);
            }
        });
    };
    this.confignavbar = function(state){
        if(state){
            $('.navbar').addClass('navbar-fixed-top');
            $('.navbar').removeClass('navbar-static-top');
            $('.wrap').css('padding-top', '70px');
        } else {
            $('.navbar').addClass('navbar-static-top');
            $('.navbar').removeClass('navbar-fixed-top');
            $('.wrap').css('padding-top', '0px');
        }
    };
}
// KeyApp y KeyJS para conexión con Parse
Parse.initialize("Ttx8LWVK1UJplEFcAHUiWH94cfTYIboVElIMeA8C", "ZL8ifGR1C3Xi0YsOnkR4agvUxuRnHTL4h1OWVdde");
//Call objects
var _news = new aos_news(); _news._init();
var _user = new aos_user(); _user._init();
var _controllers = new aos_controllers(); _controllers._init();
var main = new aos_main(); main._init();