function aos_user(){
	var _self = this;
	/**
     * [_init Constructor por defecto]
     * @return {[type]} [description]
     */
	this._init = function(){
		console.log('Constructor User Class');
	};
	/**
	 * [register Registra al usuario en el backend]
	 * @param  {[type]} $scope    	 [Scope de AngularJS]
	 * @param  {[type]} $routeParams [routeParams de AngularJS]
	 * @return {[type]}              [description]
	 */
	this.register = function($scope, $routeParams){
		$('form#signupform').validator().on('submit', function(e) {
			e.preventDefault();
			$('.loading_fullscreen').show();
			var username = $("#username").val();
			var password = $("#password").val();
			
			var fullname = $("#fullname").val();
			var mobile = $("#mobile").val();
			var email = $("#email").val();

			var user = new Parse.User();
			user.set("username", username);
			user.set("password", password);

			user.set("fullname", fullname);
			user.set("mobile", mobile);
			user.set("email", email);

			user.signUp(null, {
				success: function(user) {
					window.location.href = '#/';
					window.location.reload();
				},
				error: function(user, error) {
					console.log(error);
					bootbox.alert(error.message);
					$('.loading_fullscreen').hide();
				}
			});
			$("#signup .button").attr("disabled", "disabled");
		});
	};
	/**
	 * [logIn Identifica a un usuario en el sistema]
	 * @param  {[type]} $scope    [Scope de AngularJS]
	 * @return {[type]}   [description]
	 */
	this.logIn = function($scope) {
		$('#loginbox form').on('submit', function(e) {
			e.preventDefault();
			$('.loading_fullscreen').show();
			var username = $("#login-username").val();
			var password = $("#login-password").val();

			Parse.User.logIn(username, password, {
				success: function(user) {
					$(".error").html('').hide();
					window.location.href = '#/';
					window.location.reload();
				},
				error: function(user, error) {
					console.log(error);
					bootbox.alert(error.message);
					$('.loading_fullscreen').hide();
				}
			});

			$("#login .button").attr("disabled", "disabled");
		});
	};
	/**
	 * [update Actualiza el perfil del usuario]
	 * @return {[type]} [description]
	 */
	this.update = function(){
		$('form#updateform').validator().on('submit', function(e) {
			e.preventDefault();
			$('.loading_fullscreen').show();

			var fullname = $("#fullname").val();
			var mobile = $("#mobile").val();
			var email = $("#email").val();

			var user = Parse.User.current();
			user.set("fullname", fullname);
			user.set("mobile", mobile);
			user.set("email", email);
			
			$('.loading_fullscreen').hide();
			bootbox.alert('Your profile was updated');
			return user.save();
		});
	};
}