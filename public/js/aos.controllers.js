function aos_controllers(){
	var _self = this;
	/**
     * [_init Constructor por defecto]
     * @return {[type]} [description]
     */
	this._init = function(){
		console.log('Constructor controllers Class');
	};
	/**
	 * [get Configura los controladores de AngularJS]
	 * @param  {[type]} ctrl [ID del App]
	 * @return {[type]}      [description]
	 */
	this.get = function(ctrl){
		ctrl.config(['$routeProvider',
			function($routeProvider) {
				$routeProvider
				.when('/', {
					templateUrl: function(urlattr){
						var view = '';
						if(Parse.User.current() == null){
							view = 'view/home.html';
						} else {
							view = 'view/user_home.html'
						}
						return view;
					},
					controller: 'HomeController'
				})
				.when('/Register', {
					templateUrl: 'view/user_register.html',
					controller: 'RegisterController'
				})
				.when('/Login', {
					templateUrl: 'view/user_login.html',
					controller: 'LoginController'
				})
				.when('/Profile', {
					templateUrl: 'view/user_profile.html',
					controller: 'ProfileController'
				})
				.otherwise({
					redirectTo: '/'
				});
			}
		]);

		ctrl.controller('HomeController', function($scope) {
			main.confignavbar(true);
			user = Parse.User.current();
			if(user){
				$scope.username = user.attributes.username;
				$scope.email = user.attributes.email;

				$('form#news').on('submit', function(e) {
					e.preventDefault();
					_news.addNews($scope);
				});
			}
			setTimeout(function(){
				if(user){
					$('.in-u').show();
					$('.out-u').hide();
				} else {
					$('.in-u').hide();
					$('.out-u').show();
				}
			},150);
			if($scope.news){
				setTimeout(function(){
					$('.cnt-news').addClass('show');
				}, 150);
			}
		});

		ctrl.controller('LoginController', function($scope) {
			main.confignavbar(false);
	    	_user.logIn($scope);
		});
 
		ctrl.controller('RegisterController', function($scope, $routeParams) {
			main.confignavbar(false);
			_user.register($scope, $routeParams);
		});
		ctrl.controller('ProfileController', function($scope, $routeParams) {
			main.confignavbar(false);
			_user.update();
		});
		
		ctrl.controller('newserCtrl', function ($scope) {
			user = Parse.User.current();
			if(user){
				$scope.username = user.attributes.username;
				$scope.fullname = user.attributes.fullname;
				$scope.mobile = user.attributes.mobile;
				$scope.email = user.attributes.email;
			}
			setTimeout(function(){
				if(user){
					$('.in-u').show();
					$('.out-u').hide();
				} else {
					$('.in-u').hide();
					$('.out-u').show();
				}
			},150);
			_news.get($scope, 0);
			$('.loading_fullscreen').hide();
		});
	};
}