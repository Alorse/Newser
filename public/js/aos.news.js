function aos_news($scope){
	self = this;
	this.newsl = [];
	this.skip = 0;
	this.numpost = 15;
	/**
	 * [_init Constructor por defecto]
	 * @return {[type]} [description]
	 */
	this._init = function(){
		console.log('Constructor News Class');
		self.hola();
	};
	/**
	 * [get Obtiene y renderiza los newsers (post/entradas) desde el backend]
	 * @param  {[type]} $scope [Scope de AngularJS]
	 * @param  {[type]} skip   [description]
	 * @return {[type]}        [description]
	 */
	this.get = function($scope, skip){
		$scope.news = [];
		$scope.loadMore = function() {
			var c_show = 1;
			var GameScore = Parse.Object.extend("News");
			var query = new Parse.Query(GameScore);
			query.descending("createdAt");
			query.skip(self.skip);
			query.limit(self.numpost);
			query.find({
				success: function(myObject) {
					if(myObject.length > 0){
						myObject.forEach(function(a){
							var item = {
								id: a.id,
								description: a.attributes.description,
								user: a.attributes.user,
								username: a.attributes.username,
								time: a.createdAt
							};
							self.newsl.push(item);
							setTimeout(function(){
								$('#' + a.id).addClass('show');
							}, 150 * c_show);
							c_show++;
							$scope.news.push(item);
						});
					} else {
						console.log('no more newsers');
					}
					self.skip += self.numpost;
					$('.loading').hide();
				},
				error: function(myObject, error) {
					console.log(myObject);
					console.log(error);
					$('.loading').hide();
				}
			});
		};
	};
	/**
	 * [addNews Agrega un nuevo post a la base de datos]
	 */
	this.addNews = function(){
		var GameScore = Parse.Object.extend("News");
		var gameScore = new GameScore();
		var desc = $(".news textarea").val();
		gameScore.set("user", Parse.User.current());
		gameScore.set("username", Parse.User.current().attributes.username);
		gameScore.set("description", desc);

		gameScore.save(null, {
		  success: function(gameScore) {
		    // Execute any logic that should take place after the object is saved.
		    $(".news textarea").val('');
		    $(".parent-news").prepend('<div class="col-sm-6 col-md-4 cnt-news ng-scope " id="_new"><div class="thumbnail"><div class="caption"><h3 class="ng-binding">' + Parse.User.current().attributes.username + ' said:</h3><div class="time ng-binding">' + new Date() + '</div><p class="ng-binding">' + desc + '</p></div></div></div>');
		  	setTimeout(function(){
				$('#_new').addClass('show');
			}, 150);
		  },
		  error: function(gameScore, error) {
		    // Execute any logic that should take place if the save fails.
		    // error is a Parse.Error with an error code and message.
		    bootbox.alert('Failed to create new object, with error code: ' + error.message);
		  }
		});
	};
	this.hola = function(){
		var GameScore = Parse.Object.extend("Migui");
var gameScore = new GameScore()
		gameScore.set("username", "Miguel");
		gameScore.set("description","loqeu sea");
gameScore.set("fecha",new Date());

		gameScore.save(null, {
		  success: function(response) {
console.log(response);
		  },
		  error: function(gameScore, error) {
		    alert('Failed to create new object, with error code: ' + error.message);
		  }
		});
	};
}